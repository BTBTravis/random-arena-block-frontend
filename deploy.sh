#!/bin/bash

function print_header()
{
cat <<"EOF"

     _            _             _             
  __| | ___ _ __ | | ___  _   _(_)_ __   __ _ 
 / _` |/ _ \ '_ \| |/ _ \| | | | | '_ \ / _` |
| (_| |  __/ |_) | | (_) | |_| | | | | | (_| |
 \__,_|\___| .__/|_|\___/ \__, |_|_| |_|\__, |
           |_|            |___/         |___/ 

EOF
}

echo "Plese enter a version number, ex 0.1.0"
read version
print_header
npm run build
for x in $(ls ./public/build) ; do aws s3 cp ./public/build/"$x" s3://travisshears.apps/apps/random_arena_block/"$version"/"$x" --profile personal ; done