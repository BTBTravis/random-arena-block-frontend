import App from './App.svelte';

const rootEl = document.getElementById('random-arena-block');

const app = new App({
	target: rootEl
	// props: {
	// 	name: 'world'
	// }
});

export default app;